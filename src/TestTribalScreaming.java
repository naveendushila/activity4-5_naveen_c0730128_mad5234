import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTribalScreaming {

/*
 		Requirements for the Tribal Screaming App
		1. One person is amazing
		2. Nobody is listening
		3. Peter is shouting
		4. Two (2) people are amazing
		5. More than 2 people are amazing
		6. Shouting a lot of people
*/
	
	/*
	 * Re-factor - 1 - One Person name
	 */
	@Test
	public void checkOneName() {
		TribalScreaming name = new TribalScreaming();
		String result = name.scream("Peter");
		assertEquals("Peter is amazing", result);
	}
	
	/*
	 * Re-factor - 2 - Check Null Input
	 */
	@Test
	public void checkNullInput() {
		TribalScreaming input = new TribalScreaming();
		String result = input.scream("null");
		assertEquals("You is amazing", result);
	}
	
	/*
	 * Re-factor - 3 - Check User is shouting
	 */
	@Test
	public void checkUserIsShouting() {
		TribalScreaming name = new TribalScreaming();
		String result = name.scream("PETER");
		assertEquals("PETER IS AMAZING", result);
	}
	
	/*
	 * Re-factor - 4 - Check Two name and scream
	 */
	@Test
	public void checkTwoUsersScream() {
		TribalScreaming name = new TribalScreaming();
		String result = name.scream("Peter", "Jigisha");
		assertEquals("Peter and Jigisha are amazing", result);
	}

	/*
	 * Re-factor - 5 - Check More than two people are amazing
	 */
	@Test
	public void checkMultipleUsersAmazing() {
		TribalScreaming name = new TribalScreaming();
		String result = name.scream(new String[]{"Peter","Jigesha","Marcos","Pritesh"});
		//System.out.print("Multi User Amazing: " + result);
		assertEquals("Peter, Jigesha, Marcos, and Pritesh are amazing.", result);
	}

	/*
	 * Re-factor - 6 - Screaming a lot of people
	 */
	@Test
	public void checkMultipleUsersScreaming() {
		TribalScreaming name = new TribalScreaming();
		String result = name.scream(new String[]{"Peter","JIGESHA","Marcos","Pritesh"});
		//System.out.print("Multi Scream: " + result);
		assertEquals("Peter, Marcos, and Pritesh are amazing. JIGESHA ALSO", result);
	}
	
}
