
public class TribalScreaming {

	/*
		Requirements for the Tribal Screaming App
		1. One person is amazing
		2. Nobody is listening
		3. Peter is shouting
		4. Two (2) people are amazing
		5. More than 2 people are amazing
		6. Shouting a lot of people
	 */
	
	
	
	/*
	 * Re-factor phase for Requirement 1 and 3  
	 */

	public String scream(String name) {
		String output = "";
		if (isAllUpperCase(name)) {
			output = name + " IS AMAZING";
		}
		else if (name.equalsIgnoreCase("null")) {
			output = "You is amazing";
		}
		else {
			output = name + " is amazing";
		}
		
		return output;
	}
		
	
	/*
	 * Re-factor phase for Requirement 4  
	 */
	
	public String scream(String name1, String name2) {
		String output = "";
		output = name1 + " and " + name2 + " are amazing";
		return output;
	}
	
	
	
	/*
	 * Re-factor phase for Requirement 5 & 6  
	 */
	
	public String scream(String[] names) {
		
		int lenght = names.length;
		String output = "";
		String shout = "";
		boolean shoutCheck = false;
		
		for (int i=0; i<lenght; i++) {
			if (!isAllUpperCase(names[i])) {
				
				if (i==0) {
					output = names[i];
					output = output.concat(", ");
					
				}
				else if (i==lenght-1) {
					output = output.concat("and ");
					output = output.concat(names[i]);
					output = output.concat(" are amazing.");
				}
				else {
					output = output.concat(names[i]);
					output = output.concat(", ");
				}
			}
			else {
				shout = names[i];
				shoutCheck = true;
			}
		}
		
		if (shoutCheck)
		{
			output = output.concat(" " + shout + " ALSO");
			return output;
		}
		else {
			return output;	
		}
		
	}
	
	
	public static boolean isAllUpperCase(String s)
	{
	    for (int i=0; i<s.length(); i++)
	    {
	        if (!Character.isUpperCase(s.charAt(i)))
	        {
	            return false;
	        }
	    }
	    return true;
	}
	
}
